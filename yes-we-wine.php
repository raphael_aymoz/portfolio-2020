<!doctype html>
<html lang="fr">

  <?php include('files/includes/head.php');?>

  <body id="page--scrolling">
    <?php include('files/includes/navbar__top.php');?> 
    
    <div class="container">
        <div class="container__wrapper">
            <div class="title">
                <h2>Yes We Wine</h2>
            </div>
            <div class="description">
                <h4 class="m--top">CONCEPT</h4>
                <p>Par groupe de 10, nous avons eu 6 mois pour créer from scratch l'identité graphique de Yes We Wine. L'application a pour but de rendre le vin accessible à tous en proposant une manière très simple et intuitive de déguster du vin. <br>Nous avons mis en place toutes les maquettes pour la PWA et l'app iOS.</p>
                <h4 class="m--top">DATE</h4>
                <p>Septembre 2018 — Avril 2019</p>
                <h4 class="m--top">MISSIONS</h4>
                <ul>
                    <li>
                        <p>Création de l'identité artistique de la marque</p> 
                    </li>
                    <li>
                        <p>Création du logo</p> 
                    </li>
                    <li>
                        <p>Création des visuels et illustrations</p> 
                    </li>
                    <li>
                        <p>Création maquettes PWA et application iOS</p> 
                    </li>
                    <li>
                        <p>Motion Design</p> 
                    </li>
                </ul>
                <h4 class="m--top">OUTILS</h4>
                <p>Illustrator, Photoshop, Adobe XD, Axure, After Effects</p>
            </div>
            <div class="button m--top">
                <a href="https://apps.apple.com/fr/app/yes-we-wine/id1492044214" class="a--btn">APPLICATION iOS</a>
                <a href="https://yeswewine.fr" class="a--btn">SITE WEB</a>
            </div>
            <div class="visuel m--top">

            </div>
        </div>
    </div>



    <?php include('files/includes/script.php'); ?> 
  </body>
</html>