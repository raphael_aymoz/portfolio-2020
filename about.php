<!doctype html>
<html lang="fr">

  <?php include('files/includes/head.php');?>

  <body>

<?php include('files/includes/navbar__top.php'); ?>
<div id="fullpage">
  <div id="description" class="section">
    <div class="wrapper">
      <div class="wrapper__description">
        <h4>Hello, je suis Raphaël Aymoz.</h4><br><br>
        <p>Je suis UX/UI Designer basé à Bordeaux.</p><br>
        <p>J'ai étudié à 
          <a href="https://digital-campus.fr" target="_blank" rel="noopener noreferrer" class="a--link">Digital Campus</a>. <br>
        J'aime créer des interfaces mobiles et web puis les développer par la suite. J'adore connaître tous les aspects d'un projet pour bien communiquer au sein de l'équipe.
      
        </p>
        <p>Retrouvez-moi sur 
          <a href="https://www.instagram.com/raphaelaymoz" target="_blank" rel="noopener noreferrer" class="a--link">Instagram</a>,
          <a href="https://www.dribbble.com/aymoz" target="_blank" rel="noopener noreferrer" class="a--link">Dribbble</a> et
          <a href="https://www.linkedin.com/in/raphaël-aymoz-494916153/" target="_blank" rel="noopener noreferrer" class="a--link">Linkedin</a>.
        </p> 
        <br><br>
      </div>
      <div class="wrapper__btn">
        <p>Scroller pour voir mes compétences</p>
      </div>
    </div>
  </div>
  <div id="competences" class="section">
    <div class="wrapper">

      <div class="wrapper__description">
        <h4>Mes compétences</h4>
        <div class="wrapper__description--graphic">
          <h3>Compétences graphiques</h3>
          <ul>
            <li><img class="icons" src="files/img/icons/adobe-photoshop.svg"></li>
            <li><img class="icons" src="files/img/icons/adobe-illustrator.svg"></li>
            <li><img class="icons" src="files/img/icons/adobe-indesign.svg"></li>
            <li><img class="icons" src="files/img/icons/adobe-lightroom.svg"></li>
            <li><img class="icons" src="files/img/icons/adobe-after-effects.svg"></li>
            <li><img class="icons" src="files/img/icons/adobe-premiere.svg"></li>
          </ul>
        </div>
        <div class="wrapper__description--tech">
          <h3>Compétences techniques</h3>
          <ul>
            <li><img class="icons" src="files/img/icons/html.svg"></li>
            <li><img class="icons" src="files/img/icons/css.svg"></li>
            <li><img class="icons" src="files/img/icons/javascript.svg"></li>
            <li><img class="icons" src="files/img/icons/sass.svg"></li>
            <li><img class="icons" src="files/img/icons/visual-studio.svg"></li>
            <li><img class="icons" src="files/img/icons/sketch.svg"></li>
            <li><img class="icons" src="files/img/icons/origami.svg"></li>
          </ul>
        </div>
        <div class="wrapper__description--marketing">
          <h3>Compétences marketing</h3>
          <ul>
            <li><img class="icons" src="files/img/icons/mailchimp.svg"></li>
            <li><img class="icons" src="files/img/icons/adwords.svg"></li>
            <li><img class="icons" src="files/img/icons/shopify.svg"></li>
            <li><img class="icons" src="files/img/icons/facebook.svg"></li>
          </ul>
        </div>
        <div class="wrapper__description--others">
          <h3>Compétences diverses</h3>
          <ul>
            <li><img class="icons" src="files/img/icons/ableton.svg"><span class="paragraph">Production musicale</span></li>
          </ul>
        </div>
      </div>
      <div class="wrapper__btn">
        <p>Scroller pour découvrir qui je suis</p>
      </div>
    </div>
  </div>
</div>



  <?php include('files/includes/script.php'); ?>

<script>

  new fullpage('#fullpage', {
    autoScrolling: true
  })

</script>

</body>
</html>
