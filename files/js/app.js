$(function()
{
  const body = document.body;
  const docEl = document.documentElement;

  var auto = true;
  var uSpeed = .5;
  var uVolatility = .2;
  
  var materials = [];

  

    const MathUtils = {
        lineEq: (y2, y1, x2, x1, currentVal) => {
            // y = mx + b
            var m = (y2 - y1) / (x2 - x1), b = y1 - m * x1;
            return m * currentVal + b;
        },
        lerp: (a, b, n) =>  (1 - n) * a + n * b,
        distance: (x1, x2, y1, y2) => {
            var a = x1 - x2;
            var b = y1 - y2;
            return Math.hypot(a,b);
        }
    };
  
  
  $('div.blotter').each(function (index, value) 
  { 
    buildBlotter( $(this).attr('id') );
  });
  
  function buildBlotter(id)
  {
    var string = $('#'+id).text();
    var weight = $('#'+id).attr('data-weight');
    var size = $('#'+id).attr('data-size');
  
    var text = new Blotter.Text( string, 
    {
        family : "'HagridText-Regular',sans-serif",
        weight: 400,
        size : 80,
        padding : 100,
    });

    var material = new Blotter.LiquidDistortMaterial();
    material.uniforms.uSpeed.value = uSpeed;
    material.uniforms.uVolatility.value = uVolatility;

    materials.push(material);

    var blotter = new Blotter(material, { texts : text });

    var elem = document.getElementById(id);
    var scope = blotter.forText(text);
    
    scope.appendTo(elem);
  }
  
  
  
    $('#toggle').on('click', function(){
          
    if(auto)
      {        
        for(var i=0; i<materials.length; i++){
          materials[i].uniforms.uSpeed.value = 0;
          materials[i].uniforms.uVolatility.value = 0;     
        }
        auto = false;
      }
    else
      {
        for(var j=0; j<materials.length; j++){
               materials[j].uniforms.uSpeed.value = uSpeed;
          materials[j].uniforms.uVolatility.value = uVolatility; 
          }
        auto = true;
      }
         
  });
 
  
  
  
      const getMousePos = (ev) => {
        
              if(!auto){

                
        let posx = 0;
        let posy = 0;
        if (!ev) ev = window.event;
        if (ev.pageX || ev.pageY) {
            posx = ev.pageX;
            posy = ev.pageY;
        }
        else if (ev.clientX || ev.clientY) 	{
            posx = ev.clientX + body.scrollLeft + docEl.scrollLeft;
            posy = ev.clientY + body.scrollTop + docEl.scrollTop;
        }
        
        var obj = {x: posx, y: posy};
        
        //console.log('obj: ', {x: posx, y: posy});
        return obj;
                
              }
    }

      
     let winsize;
    const calcWinsize = () => winsize = {width: window.innerWidth, height: window.innerHeight};
    calcWinsize();
    window.addEventListener('resize', calcWinsize);
  
    let mousePos = {x: winsize.width/2, y: winsize.height/2};
    window.addEventListener('mousemove', ev => mousePos = getMousePos(ev));


    var start = null;

  
        let lastMousePosition = {x: winsize.width/2, y: winsize.height/2};
        let volatility = 0;
  
    function render(timestamp) {
      if (!start) start = timestamp;
      var progress = timestamp - start;


      if (progress > 10) {
            start = null;

      if(!auto && mousePos){
        
            const docScrolls = {left : body.scrollLeft + docEl.scrollLeft, top : body.scrollTop + docEl.scrollTop};
      const relmousepos = {x : mousePos.x - docScrolls.left, y : mousePos.y - docScrolls.top };
      const mouseDistance = MathUtils.distance(lastMousePosition.x, relmousepos.x, lastMousePosition.y, relmousepos.y);

      volatility = MathUtils.lerp(volatility, Math.min(MathUtils.lineEq(0.9, 0, 100, 0, mouseDistance),0.9), 0.05);

        //console.log('volatility: ', volatility);
        
        
        for(var j=0; j<materials.length; j++){
          materials[j].uniforms.uSpeed.value = uSpeed;
          materials[j].uniforms.uVolatility.value = volatility; 
        }
        
      lastMousePosition = {x: relmousepos.x, y: relmousepos.y};
        
      }
            
  
      
      
      }
      window.requestAnimationFrame(render);
    }

    window.requestAnimationFrame(render);
  
  
});