<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Raphaël Aymoz — UX/UI designer</title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Hello! Je suis Raphael Aymoz, designer full stack basé à Bordeaux." />
    <meta name="keywords" content="UX UI designer, graphic designer, web designer, Bordeaux, Raphael Aymoz" />
    <!--
    /**
    * @license
    * MyFonts Webfont Build ID 3857781, 2020-01-18T06:37:12-0500
    * 
    * The fonts listed in this notice are subject to the End User License
    * Agreement(s) entered into by the website owner. All other parties are 
    * explicitly restricted from using the Licensed Webfonts(s).
    * 
    * You may obtain a valid license at the URLs below.
    * 
    * Webfont: HagridText-Regular by Zetafonts
    * URL: https://www.myfonts.com/fonts/zetafonts/hagrid/text-regular/
    * Copyright: Copyright © 2019 by Cosimo Lorenzo Pancini. All rights reserved.
    * Licensed pageviews: Unlimited
    * 
    * 
    * 
    * © 2020 MyFonts Inc
    */
    -->
    <!-- <link rel="stylesheet" type="text/css" href="MyFontsWebfontsKit.css"> -->
    <link href="https://fonts.googleapis.com/css?family=DM+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli:400,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="files/css/app.css">
    <link rel="stylesheet" href="files/css/jquery.pagepiling.css">
    <link rel="icon" href="img/favicon.png" type="img/png" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.7/fullpage.min.css" integrity="sha256-3IkGqGYOvq1Ype2MXFwVJFeBtBACgiveho3SacOEEP8=" crossorigin="anonymous" />

    <script src="https://kit.fontawesome.com/9d28390c68.js" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>

</head>