<!doctype html>
<html lang="fr">

  <?php include('files/includes/head.php');?>

  <body id="page">

  <?php include('files/includes/navbar__top.php');?> 
    
    <div id="canvas" class="logo blotter" data-weight="100" data-size="270">
      Design Interface &nbsp; &nbsp;
    </div>

    <div class="footer">
      <div class="footer__lab">
        <a href="lab.php" class="btn">Le Lab</a>
      </div>
      <div class="footer__toggle">
        <label class="switch">
          <span class="text">désactiver l'animation</span>
          <input id="toggle" type="checkbox">
          <span></span>
        </label>
      </div>
      <div class="footer__link">
        <a href="https://dribbble.com/aymoz" target="_blank" rel="noopener noreferrer" class="btn">dribbble</a>
        <a href="https://www.linkedin.com/in/rapha%C3%ABl-aymoz-494916153/" target="_blank" rel="noopener noreferrer" class="btn">linkedin</a>
      </div>
    </div>
    <?php include('files/includes/script.php'); ?> 
  </body>
</html>
