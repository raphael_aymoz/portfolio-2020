<!doctype html>
<html lang="fr">

<?php include('files/includes/head.php');?>

<body>
  <div class="navbar2">
    <div class="logo">
      <a href="index.php" class="btn">raphaël aymoz</a>
    </div>
    <div class="works--page">
      <a href="works.php" class="btn">works</a>
    </div>
    <div class="about">
      <a href="about.php" class="btn">about</a>
    </div>
  </div>
  <div id="pagepiling">
    <div class="section">
      <div class="wrapper2">

        <div class="title">
          <a href="#" target="_blank" rel="noopener noreferrer" class="a--heading">Very Bad Geek</a><br>
          <h3 class="m--top ">Stage Webdesign + Webmarketing — 6 mois</h3>
          <a href="very-bad-geek.php" class="a--link m--top">Voir le projet</a>
        </div>
          
        <div class="footer">
          <div class="footer--year">
            <h2>1/4</h2>
          </div>
          <div class="footer--more">
            <h2>défilez pour voir plus</h2>
          </div>
        </div>
        <div class="cover-project">
          <a href="very-bad-geek.php" rel="noopener noreferrer">
            <img src="files/img/very-bad-geek/vbgcover2.jpg">
            <img src="files/img/very-bad-geek/vbgcover.png">
          </a>
        </div>

      </div>
    </div>
    <div class="section">
      <div class="wrapper2">
        <div class="title">
          <a href="#"target="_blank" rel="noopener noreferrer" class="a--heading">Yes We Wine</a><br>
          <h3 class="m--top ">Projet École — 6 mois</h3>
          <a href="yes-we-wine.php" class="a--link m--top">Voir le projet</a>
        </div>
        <div class="footer">
          <div class="footer--year">
            <h2>2/4</h2>
          </div>
          <div class="footer--more">
            <h2>défiler pour voir plus</h2>
          </div>
        </div>
        <div class="cover-project">
          <a href="yes-we-wine.php" target="_blank" rel="noopener noreferrer">
            <img src="files/img/yes-we-wine/ywwillustrations.png">
            <img src="files/img/yes-we-wine/ywwcover.png">
          </a>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="wrapper2">
        <div class="title">
          <a href="#" target="_blank" rel="noopener noreferrer" class="cool-link">LIEN SITE WEB</a><br>
          <h3>heading : description projet</h3>
        </div>
        <div class="footer">
          <div class="footer--year">
            <h2>3/4</h2>
          </div>
          <div class="footer--more">
            <h2>défiler pour voir plus</h2>
          </div>
        </div>
        <div class="cover-project">
          <a href="#" target="_blank" rel="noopener noreferrer">
          <img src="https://picsum.photos/200/300">
          <img src="https://picsum.photos/200">
          </a>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="wrapper2">
        <div class="title">
          <a href="#" target="_blank" rel="noopener noreferrer" class="cool-link">LIEN SITE WEB</a><br>
          <h3>heading : description projet</h3>
        </div>
        <div class="footer">
          <div class="footer--year">
            <h2>4/4</h2>
          </div>
          <div class="footer--more">
            <h2>défiler pour revenir au début</h2>
          </div>
        </div>
        <div class="cover-project">
          <a href="#" target="_blank" rel="noopener noreferrer">
          <img src="https://picsum.photos/200/300">
          <img src="https://picsum.photos/200">
          </a>
        </div>
      </div>
    </div>
  </div>

  <script src="files/js/jquery-3.4.1.min.js"></script>
  <script src="files/js/jquery.pagepiling.min.js"></script>
  <script>
    //work page scroll
    $(document).ready(function() {
      $('#pagepiling').pagepiling({
        direction: 'vertical',
        scrollingSpeed: 250,
        easing: 'swing',
        loopBottom: true,
        normalScrollElementTouchThreshold: 5,
        touchSensitivity: 5,
        keyboardScrolling: true,
        sectionSelector: '.section',
      });
    });
  </script>

  </body>
</html>
